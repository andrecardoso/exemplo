package ccti.pmbv.sistemas.blog.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ccti.pmbv.sistemas.blog.service.FileUploadService;

@WebServlet("/arquivo-servlet")
public class FileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private FileUploadService fileService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nome = req.getParameter("nome");
		byte[] bytes = fileService.recuperar(nome);
		
		resp.setHeader("Content-Type", "image/*");
		resp.setContentLength(bytes.length);
		
		resp.getOutputStream().write(bytes);
		resp.getOutputStream().flush();

	}
	
}