package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.repository.NoticiaRepository;


@Named
@ViewScoped
public class SelecaoNoticiaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private NoticiaRepository produtos;
	
	private String nome;
	
	private List<Noticia> noticiasFiltradas;
	
	public void pesquisar() {
		noticiasFiltradas = produtos.porTitulo(nome);
	}

	public void selecionar(Noticia produto) {
		RequestContext.getCurrentInstance().closeDialog(produto);
	}
	
	public void abrirDialogo() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("modal", false);
		opcoes.put("showHeader", false);
		opcoes.put("draggable", false);
		opcoes.put("resizable", false);
		opcoes.put("minimizable", true);
		opcoes.put("responsive", true);
		opcoes.put("closeOnEscape", true);
		opcoes.put("responsive", true);	
		opcoes.put("fitViewport", true);
		opcoes.put("showEffect", "fade");
		opcoes.put("hideEffect", "fade");
		opcoes.put("minHeight", 300);
		opcoes.put("height", 400);		
					
		
		RequestContext.getCurrentInstance().openDialog("/dialogos/SelecaoNoticia", opcoes, null);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Noticia> getNoticiasFiltradas() {
		return noticiasFiltradas;
	}

}