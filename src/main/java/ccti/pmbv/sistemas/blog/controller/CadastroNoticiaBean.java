package ccti.pmbv.sistemas.blog.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Categoria;
import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.CategoriaRepository;
import ccti.pmbv.sistemas.blog.repository.UsuarioRepository;
import ccti.pmbv.sistemas.blog.service.FotoService;
import ccti.pmbv.sistemas.blog.service.NoticiaService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroNoticiaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Noticia noticia;
	private Usuario usuario;
	private Categoria categoriaPai;
	private List<Categoria> categoriasRaizes;
	private List<Categoria> subcategorias;

	private List<Usuario> usuarios;

	private String color = "#33fc14";

	@Inject
	private NoticiaService noticiaService;

	@Inject
	private UsuarioRepository usuarioRepository;

	@Inject
	private CategoriaRepository categorias;

	@Inject
	private FotoService fotoService;

	public CadastroNoticiaBean() {
		limpar();
	}

	public void inicializar() {
		if (this.noticia == null) {
			limpar();
		}

		usuarios = usuarioRepository.porTodos();

		categoriasRaizes = categorias.raizes();

		if (this.categoriaPai != null) {
			carregarSubcategorias();

		}
	}

	public void carregarUsuarios() {
		usuarios = usuarioRepository.porTodos();
	}

	public void carregarSubcategorias() {
		subcategorias = categorias.subcategoriasDe(categoriaPai);
	}

	private void limpar() {
		this.noticia = new Noticia();
		this.usuario = new Usuario();
	}

	public void salvar() {
		try {
			this.noticia = noticiaService.salvar(this.noticia);
			limpar();
			FacesUtil.addInfoMessage("Notícia salva com sucesso!");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
			ne.printStackTrace();
		}

	}

	public Noticia getNoticia() {
		return noticia;
	}

	public void setNoticia(Noticia noticia) {
		this.noticia = noticia;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<Categoria> getCategoriasRaizes() {
		return categoriasRaizes;
	}

	@NotNull
	public Categoria getCategoriaPai() {
		return categoriaPai;
	}

	public void setCategoriaPai(Categoria categoriaPai) {
		this.categoriaPai = categoriaPai;
	}

	public List<Categoria> getSubcategorias() {
		return subcategorias;
	}

	public String getColor() {
		return color;
	}

	public void setColor(final String color) {
		this.color = color;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void changeColor() {
		if (color.equals("#1433FC")) {
			color = "#33fc13";
		} else {
			color = "#1433FC";
		}
	}

	public void upload(FileUploadEvent event) {
		UploadedFile uploadedFile = event.getFile();

		try {
			fotoService.deletar(noticia.getFoto());

			String foto = fotoService.salvarFotoTemp(uploadedFile.getFileName(), event.getFile().getContents());
			noticia.setFoto(foto);
		} catch (Exception e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
	}

	public void removerFoto() {
		try {
			fotoService.deletarTemp(noticia.getFoto());
		} catch (IOException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}

		noticia.setFoto(null);
	}

	public boolean isEditando() {
		return this.noticia.getId() != null;
	}

	public void adicionarUsuarios() throws NegocioException {

		//this.noticia.adicionarUsuarios(usuario);
		this.usuarios.remove(usuario);

	}

	public void removerUsuarios() {

		//this.noticia.removerUsuarios(usuario);
		this.usuarios.add(usuario);
	}
}