package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Categoria;
import ccti.pmbv.sistemas.blog.repository.CategoriaRepository;
import ccti.pmbv.sistemas.blog.repository.filtros.CategoriaFilter;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;


@Named
@ViewScoped
public class PesquisaCategoriasBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CategoriaRepository categorias;
	
	private CategoriaFilter filtro;
	private List<Categoria> categoriasFiltradas;
	
	private Categoria categoriaSelecionada;
	
	public PesquisaCategoriasBean() {
		filtro = new CategoriaFilter();
	}
	
	public void pesquisar() {
		categoriasFiltradas = categorias.filtrados(filtro);
	}
	
	public void excluir() {
		try {
			categorias.remover(categoriaSelecionada);
			categoriasFiltradas.remove(categoriaSelecionada);
			
			FacesUtil.addInfoMessage("Categoria " + categoriaSelecionada.getDescricao() + 
					" excluída com sucesso!");
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
	}
	
	public List<Categoria> getCategoriasFiltradas() {
		return categoriasFiltradas;
	}

	public CategoriaFilter getFiltro() {
		return filtro;
	}

	public Categoria getCategoriaSelecionada() {
		return categoriaSelecionada;
	}

	public void setCategoriaSelecionada(Categoria categoriaSelecionada) {
		this.categoriaSelecionada = categoriaSelecionada;
	}
	
}
