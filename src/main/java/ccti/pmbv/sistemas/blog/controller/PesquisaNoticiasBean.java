package ccti.pmbv.sistemas.blog.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.repository.NoticiaRepository;
import ccti.pmbv.sistemas.blog.repository.filtros.NoticiaFilter;
import ccti.pmbv.sistemas.blog.service.FotoService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaNoticiasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Noticia> feed;

	private NoticiaFilter filtro;

	private List<Noticia> noticiasFiltradas;

	private Noticia noticiaSelecionada;

	@Inject
	private NoticiaRepository noticias;

	@Inject
	private FotoService fotoService;
	
	
	public PesquisaNoticiasBean() {
		filtro = new NoticiaFilter();
	}

	public void inicializar() {
		feed = noticias.porTodos();
		noticiasFiltradas = new ArrayList<>();
	}

	public void pesquisar() {
		noticiasFiltradas = noticias.filtrados(filtro);
	}

	public void excluir() {
		try {
			noticias.remover(noticiaSelecionada);
			noticiasFiltradas.remove(noticiaSelecionada);
			fotoService.deletar(noticiaSelecionada.getFoto());

			FacesUtil.addInfoMessage("Notícia " + noticiaSelecionada.getTitulo() + " excluída com sucesso!");
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Noticia> getNoticiasFiltradas() {
		return noticiasFiltradas;
	}

	public NoticiaFilter getFiltro() {
		return filtro;
	}

	public Noticia getNoticiaSelecionada() {
		return noticiaSelecionada;
	}

	public void setNoticiaSelecionada(Noticia noticiaSelecionada) {
		this.noticiaSelecionada = noticiaSelecionada;
	}

	public List<Noticia> getFeed() {
		return feed;
	}

	public void setFeed(List<Noticia> feed) {
		this.feed = feed;
	}

	public List<Noticia> getNoticiasFiltradas(Long id) {
		return noticiasFiltradas = noticias.findById(id);
	}

}
