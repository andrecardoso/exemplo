package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Grupo;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.GrupoRepository;
import ccti.pmbv.sistemas.blog.service.UsuarioService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;
	
	private DualListModel<Grupo> listaGrupos;
	
	@Inject
	private UsuarioService cadastroUsuarioService;

	@Inject
	private GrupoRepository grupos;	

	public void inicializar() {
		if (this.usuario == null) {
			limpar();
			
		} else {
			List<Grupo> lista = grupos.todos();
			lista.removeAll(usuario.getGrupos());

			listaGrupos = new DualListModel<>(lista, new ArrayList<>(usuario.getGrupos()));
		}
	}

	public void limpar() {
		usuario = new Usuario();

		listaGrupos = new DualListModel<>(grupos.todos(), new ArrayList<>());
	}

	public void salvar() {
		try {
			usuario.setGrupos(listaGrupos.getTarget());
			cadastroUsuarioService.salvar(usuario);
			FacesUtil.addInfoMessage("Usuário salvo com sucesso!");
			
			limpar();
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isEditando() {
		return this.usuario.getId() != null;
	}

	public DualListModel<Grupo> getListaGrupos() {
		return listaGrupos;
	}

	public void setListaGrupos(DualListModel<Grupo> listaGrupos) {
		this.listaGrupos = listaGrupos;
	}
}
