package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.UsuarioRepository;
import ccti.pmbv.sistemas.blog.repository.filtros.UsuarioFilter;
import ccti.pmbv.sistemas.blog.service.UsuarioService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaUsuariosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioService usuarioService;
	
	@Inject
	private UsuarioRepository usuarios;

	private UsuarioFilter filtro;
	private List<Usuario> usuariosFiltrados;

	private Usuario usuarioSelecionado;

	public PesquisaUsuariosBean() {
		filtro = new UsuarioFilter();
	}

	public void excluir() {
		try {
			usuarioService.excluir(usuarioSelecionado);
			usuariosFiltrados.remove(usuarioSelecionado);

			FacesUtil.addInfoMessage("Usuario " + usuarioSelecionado.getNome() + " excluído com sucesso.");
		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage(ne.getMessage());
		}
	}

	public void pesquisar() {
		usuariosFiltrados = usuarios.filtrados(filtro);
	}

	public List<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}

	public UsuarioFilter getFiltro() {
		return filtro;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

}