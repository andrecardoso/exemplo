package ccti.pmbv.sistemas.blog.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.model.TipoPessoa;
import ccti.pmbv.sistemas.blog.service.PessoaService;
import ccti.pmbv.sistemas.blog.service.FileUploadService;
import ccti.pmbv.sistemas.blog.service.UsuarioService;
import ccti.pmbv.sistemas.blog.util.jsf.FacesUtil;

@Named("cadastroClienteBean")
@ViewScoped
public class CadastroPessoaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private StreamedContent streamedContent;

	private UploadedFile uploadedFile;
	
	private Pessoa pessoa;

	@Inject
	private FileUploadService uploadService;
	
	@Inject
	private PessoaService pessoaService;

	@Inject
	private UsuarioService usuarioService;

	public void inicializar() {
		if (pessoa == null) {
			limpar();
		}
	}

	public void limpar() {
		this.pessoa = new Pessoa();
		this.pessoa.setTipo(TipoPessoa.FISICA);

	}

	public void salvar() {

		try {
			boolean isClienteExistente = usuarioService.verificarUsuarioExistente(pessoa.getUsuario());
			
			if (pessoa.isNovo() && isClienteExistente) {
				throw new NegocioException("Já existe um usuário com o e-mail informado.");
			}
							
			pessoaService.salvar(pessoa);
			FacesUtil.addInfoMessage("Pessoa salvo com sucesso!");
			limpar();
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}

	}

	// https://javabydeveloper.com/save-image-working-large-objects/
	public void upload(FileUploadEvent event) {		
		try {
			
			UploadedFile uploadedFile = event.getFile();
			
			uploadService.escrever(uploadedFile.getFileName(), uploadedFile.getContents());									 				
			
//			byte[] picInBytes = new byte[(int) arquivo.length()];
//			FileInputStream fileInputStream = new FileInputStream(arquivo);
//			fileInputStream.read(picInBytes);
//			fileInputStream.close();
			pessoa.setFoto(uploadedFile.getContents());						
			FacesUtil.addInfoMessage("Upload da foto completo.");
		} catch (IOException e) {
			FacesUtil.addErrorMessage("Erro ao fazer upload da foto.");
		}
	}
	
	public void download(File file) throws IOException {

		InputStream inputStream = new FileInputStream(file);

		streamedContent = new DefaultStreamedContent(inputStream, Files.probeContentType(file.toPath()),
				file.getName());
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}		

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public boolean isEditando() {
		return pessoa.getId() != null;
	}

}
