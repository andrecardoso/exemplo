package ccti.pmbv.sistemas.blog.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.UsuarioRepository;


@Named
@ViewScoped
public class SelecaoUsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarios;
	
	private String nome;
	
	private List<Usuario> usuariosFiltrados;
	
	public void pesquisar() {
		usuariosFiltrados = usuarios.porNome(nome);
	}

	public void selecionar(Usuario cliente) {
		RequestContext.getCurrentInstance().closeDialog(cliente);
	}
	
	public void abrirDialogo() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("modal", false);
		opcoes.put("resizable", false);
		opcoes.put("closeOnEscape", true);
		opcoes.put("responsive", true);
		opcoes.put("minHeight", 300);
		opcoes.put("height", 400);
					
		
		RequestContext.getCurrentInstance().openDialog("/dialogos/SelecaoUsuario", opcoes, null);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Usuario> getUsuariosFiltrados() {
		return usuariosFiltrados;
	}

}