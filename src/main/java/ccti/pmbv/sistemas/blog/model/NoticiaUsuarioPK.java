package ccti.pmbv.sistemas.blog.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NoticiaUsuarioPK implements Serializable {
	
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "noticia_id", nullable = false)
	private Long noticiaId;

	@Column(name = "usuario_id", nullable = false)
	private Long usuarioId;

	public NoticiaUsuarioPK() {
		
	}

	public Long getNoticiaId() {
		return noticiaId;
	}


	public void setNoticiaId(Long noticiaId) {
		this.noticiaId = noticiaId;
	}


	public Long getUsuarioId() {
		return usuarioId;
	}


	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noticiaId == null) ? 0 : noticiaId.hashCode());
		result = prime * result + ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NoticiaUsuarioPK other = (NoticiaUsuarioPK) obj;
		if (noticiaId == null) {
			if (other.noticiaId != null)
				return false;
		} else if (!noticiaId.equals(other.noticiaId))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}


	

}