package ccti.pmbv.sistemas.blog.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ccti.pmbv.sistemas.blog.model.Grupo;
import ccti.pmbv.sistemas.blog.model.Permissao;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.UsuarioRepository;
import ccti.pmbv.sistemas.blog.util.cdi.CDIServiceLocator;


public class MyCustomUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		UsuarioRepository usuarios = CDIServiceLocator.getBean(UsuarioRepository.class);
		Usuario usuario = usuarios.porEmail(email);
		
		UsuarioSistema user = null;
		
		if (usuario != null) {
			user = new UsuarioSistema(usuario, getAuthorities(usuario));
		} else {
			throw new UsernameNotFoundException("Usuário não foi encontrado.");
		}
		
		return user;
	}

	
	
	private Collection<? extends GrantedAuthority> getAuthorities(Usuario usuario) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		
		for (Grupo grupo : usuario.getGrupos()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + grupo.getNome().toUpperCase()));
			
			for(Permissao permissao: grupo.getPermissoes()) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + permissao.getNome().toUpperCase()));
				
			}
		}
		
		return authorities;
	}
}
