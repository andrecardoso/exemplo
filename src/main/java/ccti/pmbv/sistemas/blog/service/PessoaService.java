package ccti.pmbv.sistemas.blog.service;

import java.io.Serializable;

import javax.inject.Inject;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.model.TipoPessoa;
import ccti.pmbv.sistemas.blog.repository.PessoaRepository;
import ccti.pmbv.sistemas.blog.repository.GrupoRepository;
import ccti.pmbv.sistemas.blog.security.MyPasswordEncoder;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class PessoaService implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private PessoaRepository pessoas;
	
	@Inject
	private GrupoRepository grupos;
	
	@Transactional
	public Pessoa salvar(Pessoa pessoa) throws NegocioException {
		if (pessoa.isNovo()) {
			pessoa.setTipo(TipoPessoa.FISICA);
			pessoa.getUsuario().setNome(pessoa.getNome());
			pessoa.getUsuario().getGrupos().clear();
			pessoa.getUsuario().getGrupos().add(0, grupos.porId((long) 3));									

			
			Pessoa clienteExistente = pessoas.porCpf(pessoa.getCpf());
			
			if (clienteExistente != null && clienteExistente.getCpf().equalsIgnoreCase(pessoa.getCpf())) {
				throw new NegocioException("Já existe um cliente com o cpf informado.");
			}
			
		}
			
		pessoa.getUsuario().setSenha(MyPasswordEncoder.getPasswordEncoder(pessoa.getUsuario().getSenha()));
		
		return pessoas.guardar(pessoa);
	}
}