package ccti.pmbv.sistemas.blog.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Pessoa;
import ccti.pmbv.sistemas.blog.repository.filtros.PessoaFilter;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class PessoaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Pessoa guardar(Pessoa pessoa) throws NegocioException {
		return manager.merge(pessoa);

	}

	@Transactional
	public void remover(Pessoa pessoa) throws NegocioException {
		try {
			pessoa = porId(pessoa.getId());
			manager.remove(pessoa);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Cliente não pode ser excluído.");
		}
	}

	public Pessoa porId(Long id) {
		return this.manager.find(Pessoa.class, id);
	}

	public Pessoa porUsuario(Integer id) {
		return this.manager.createQuery("from Pessoa where usuario = :id", Pessoa.class).setParameter("id", id)
				.getSingleResult();
	}

	public Pessoa porCpf(String cpf) {
		Pessoa pessoa = null;
		
		try {
			pessoa = this.manager.createQuery("from Pessoa where cpf like :cpf", Pessoa.class)
			.setParameter("cpf", cpf).getSingleResult();
		} catch (NoResultException e) {
			// nenhum cliente encontrado com o cpf informado
		}

		return pessoa;
	}

	public List<Pessoa> porNome(String nome) {
		return this.manager.createQuery("from Pessoa where upper(nome) like :nome", Pessoa.class)
				.setParameter("nome", nome.toUpperCase() + "%").getResultList();
	}
	

	public List<Pessoa> filtrados(PessoaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pessoa> criteriaQuery = builder.createQuery(Pessoa.class);
		List<Predicate> predicates = new ArrayList<>();

		Root<Pessoa> clienteRoot = criteriaQuery.from(Pessoa.class);

		if (StringUtils.isNotBlank(filtro.getCpf())) {
			predicates.add(builder.equal(clienteRoot.get("cpf"), filtro.getCpf()));
		}

		if (StringUtils.isNotBlank(filtro.getNome())) {
			predicates.add(
					builder.like(builder.lower(clienteRoot.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
		}

		criteriaQuery.select(clienteRoot);
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		criteriaQuery.orderBy(builder.asc(clienteRoot.get("nome")));

		TypedQuery<Pessoa> query = manager.createQuery(criteriaQuery);
		return query.getResultList();
	}

}