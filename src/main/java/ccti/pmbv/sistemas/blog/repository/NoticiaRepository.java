package ccti.pmbv.sistemas.blog.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import ccti.pmbv.sistemas.blog.exception.NegocioException;
import ccti.pmbv.sistemas.blog.model.Noticia;
import ccti.pmbv.sistemas.blog.model.Usuario;
import ccti.pmbv.sistemas.blog.repository.filtros.NoticiaFilter;
import ccti.pmbv.sistemas.blog.util.jpa.Transactional;

public class NoticiaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Noticia porId(Long id) {
		return manager.find(Noticia.class, id);
	}
	
	public List<Noticia> findById(Long id) {
		return this.manager.createQuery("from Noticia where id = :id", Noticia.class)
				.setParameter("id", id).getResultList();
	}
	
	public List<Noticia> porTitulo(String titulo) {
		return this.manager.createQuery("from Noticia where upper(titulo) like :titulo", Noticia.class)
				.setParameter("titulo", titulo).getResultList();
	}
	
	public List<Noticia> porTodos() {
		return this.manager.createQuery("from Noticia n", Noticia.class).getResultList();
	}

	public Noticia guardar(Noticia noticia) {
		return manager.merge(noticia);

	}

	@Transactional
	public void remover(Noticia noticia) throws NegocioException {
		try {
			noticia = porId(noticia.getId());
			manager.remove(noticia);
			manager.flush();
		} catch (PersistenceException e) {
			throw new NegocioException("Noticia não pode ser excluída.");
		}
	}

	private List<Predicate> criarPredicatesParaFiltro(NoticiaFilter filtro, Root<Noticia> noticiaRoot,
			From<?, ?> usuarioJoin, From<?, ?> categoriaJoin) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(filtro.getTitulo())) {
			predicates.add(builder.like(builder.lower(noticiaRoot.get("titulo")),
					"%" + filtro.getTitulo().toLowerCase() + "%"));
		}

		if (StringUtils.isNotBlank(filtro.getTexto())) {
			predicates.add(
					builder.like(builder.lower(noticiaRoot.get("texto")), "%" + filtro.getTexto().toLowerCase() + "%"));
		}
		
		if (StringUtils.isNotBlank(filtro.getNomeAutor())) {
			predicates.add(
					builder.like(builder.lower(noticiaRoot.get("autor")), "%" + filtro.getNomeAutor().toLowerCase() + "%"));
		}


		if (filtro.getDataCriacaoDe() != null) {
			predicates.add(builder.greaterThanOrEqualTo(noticiaRoot.get("dataCriacao"), filtro.getDataCriacaoDe()));
		}

		if (filtro.getDataCriacaoAte() != null) {
			predicates.add(builder.lessThanOrEqualTo(noticiaRoot.get("dataCriacao"), filtro.getDataCriacaoAte()));
		}

		if (StringUtils.isNotBlank(filtro.getNomeAutor())) {
			predicates.add(builder.like(usuarioJoin.get("nome"), "%" + filtro.getNomeAutor() + "%"));
		}
		
		if (StringUtils.isNotBlank(filtro.getCategoria())) {
			predicates.add(builder.like(categoriaJoin.get("descricao"), "%" + filtro.getCategoria() + "%"));
		}

		return predicates;
	}

	public List<Noticia> filtrados(NoticiaFilter filtro) {
		From<?, ?> orderByFromEntity = null;

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Noticia> criteriaQuery = builder.createQuery(Noticia.class);

		Root<Noticia> noticiaRoot = criteriaQuery.from(Noticia.class);
		From<?, ?> usuarioJoin = (From<?, ?>) noticiaRoot.fetch("usuarios", JoinType.INNER);
		From<?, ?> categoriaJoin = (From<?, ?>) noticiaRoot.fetch("categoria", JoinType.INNER);

		List<Predicate> predicates = criarPredicatesParaFiltro(filtro, noticiaRoot, usuarioJoin, categoriaJoin);

		criteriaQuery.select(noticiaRoot);
		criteriaQuery.where(predicates.toArray(new Predicate[0]));
		
		
		if (filtro.getPropriedadeOrdenacao() != null) {
			String nomePropriedadeOrdenacao = filtro.getPropriedadeOrdenacao();
			orderByFromEntity = noticiaRoot;

			if (filtro.getPropriedadeOrdenacao().contains(".")) {
				nomePropriedadeOrdenacao = nomePropriedadeOrdenacao
						.substring(filtro.getPropriedadeOrdenacao().indexOf(".") + 1);
			}

			if (filtro.getPropriedadeOrdenacao().startsWith("autor.")) {
				orderByFromEntity = usuarioJoin;
			}

			if (filtro.isAscendente() && filtro.getPropriedadeOrdenacao() != null) {
				criteriaQuery.orderBy(builder.asc(orderByFromEntity.get(nomePropriedadeOrdenacao)));
			} else if (filtro.getPropriedadeOrdenacao() != null) {
				criteriaQuery.orderBy(builder.desc(orderByFromEntity.get(nomePropriedadeOrdenacao)));
			}
		}

		TypedQuery<Noticia> query = manager.createQuery(criteriaQuery);

		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());

		return query.getResultList();
	}

	public int quantidadeFiltrados(NoticiaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

		Root<Noticia> noticiaRoot = criteriaQuery.from(Noticia.class);
		Join<Noticia, Usuario> usuarioJoin = noticiaRoot.join("autor", JoinType.INNER);

		Join<Noticia, Usuario> categoriaJoin = noticiaRoot.join("categoria", JoinType.INNER);
		
		List<Predicate> predicates = criarPredicatesParaFiltro(filtro, noticiaRoot, usuarioJoin, categoriaJoin);

		criteriaQuery.select(builder.count(noticiaRoot));
		criteriaQuery.where(predicates.toArray(new Predicate[0]));

		TypedQuery<Long> query = manager.createQuery(criteriaQuery);

		return query.getSingleResult().intValue();
	}
}